from django.http import Http404

from django.http import HttpResponse, HttpResponseRedirect

from django.urls import reverse

from django.shortcuts import get_object_or_404

from .models import Task
from django.shortcuts import render

def index(request):
    return render(request, 'MyToDos/index.html')


def index(request):
    todos = Task.objects.order_by("priority")
    context = {'todos': todos}
    return render(request, 'MyToDos/index.html', context)

def new(request):
    text = request.POST.get('text', False)
    created = request.POST["pub_date"]
    deadline = request.POST["deadline"]
    status = request.POST["status"]
    priority = request.POST["priority"]
    print(text, created, deadline, status, priority)

    Task.objects.create(text = text,
                        created = created,
                        deadline = deadline,
                        status = status,
                        priority=priority)

    return HttpResponseRedirect(reverse('MyToDos:index'))


def status_change(request, todo_id):
    todo = get_object_or_404(Task, pk=todo_id)
    todo.status = request.POST["status"]
    todo.save()
    return HttpResponseRedirect(reverse('MyToDos:index'))


def detail(request, todo_id):
    todo = get_object_or_404(Task, pk= todo_id)
    return render(request, 'MyTodos/detail.html', {'todo': todo})


def delete(request, todo_id):
    todo = Task.objects.get(pk=todo_id)
    todo.delete()
    return HttpResponseRedirect(reverse('MyToDos:index'))


