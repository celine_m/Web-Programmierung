from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Payment(models.Model):
    amount = models.FloatField()
    von = models.ForeignKey(User, on_delete=models.CASCADE)
    fuer = models.ManyToManyField(User, related_name="pay_User")
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    description =models.CharField(max_length=200)


class Payment_User(models.Model):
    user = models.ForeignKey(Payment, on_delete=models.CASCADE)
    payment = models.ForeignKey(User, on_delete=models.CASCADE)



