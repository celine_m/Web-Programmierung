from django.shortcuts import render
from .models import User, Payment, Payment_User
from django.http import HttpResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from django.urls import reverse


# Create your views here.
def index(request):
    myusers = User.objects.all()
    mypayments = Payment.objects.all()
    context = {"myusers" : myusers,
               "mypayments": mypayments}

    return render(request, 'letspay/index.html', context)

def user_add(request):
    name = request.POST["name"]
    User.objects.create(name = name)

    return HttpResponseRedirect(reverse('letspay:index'))


def payment_to_user(request):
    amount = request.POST["amount"]
    user_id = request.POST.get("name")
    von = User.objects.get(pk = user_id)
    user_id = request.POST.get("adressat")
    fuer = User.objects.get(pk=user_id)

    datetime = request.POST["date"]
    description = request.POST["text"]
    print(amount, von, fuer, datetime, description)

    payment = Payment.objects.create(amount = amount,
                        von = von,
                           #fuer wird später gemacht, da das set nicht direkt angelegt werden kann
                        date_time = datetime,
                        description =description)
    payment.fuer.add(fuer)
    payment.save()
    return HttpResponseRedirect(reverse('letspay:index'))


def user_details(request, user_id):
    user_id = user_id
    user_name = User.objects.get(pk=user_id)
    context = {
        "user_id": user_id,
        "user_name": user_name,
        "mypayments": Payment.objects.filter(von=user_name)
    }
    return render(request, "letspay/details.html", context)





