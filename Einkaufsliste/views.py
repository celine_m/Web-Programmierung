from django.http import Http404

from django.http import HttpResponse, HttpResponseRedirect

from django.urls import reverse

from django.shortcuts import get_object_or_404

from .models import Shop, Item, Liste
from django.shortcuts import render

# Create your views here.

def index(request):
    mylists = Liste.objects.all()
    myshops = Shop.objects.all()
    context = {"mylists" : mylists,
              "myshops": myshops,
               }
    return render(request, 'myshoppinglist/index.html', context)

def shops(request):
    myshops = Shop.objects.all()
    context = {"myshops" : myshops}
    return render(request, "myshoppinglist/shops.html", context)

def items(request):
    myitems = Item.objects.all()
    context = {"myitems":myitems}
    return render(request, "myshoppinglist/details.html", context)


def addlist(request):
    name = request.POST.get('name')
    Liste.objects.create(name=name)
    return HttpResponseRedirect(reverse('myshoppinglist:index'))


def addshop(request):
    name = request.POST.get('name')
    Shop.objects.create(name = name)
    return HttpResponseRedirect(reverse('myshoppinglist:shops'))


def additem(request):
    name = request.POST.get('name')
    amount = request.POST.get("amount")
    status = request.POST.get("status")
    shop_id = request.POST.get("shop")
    shop = Shop.objects.get(pk = shop_id)

    list_id = request.POST.get("list")
    list = Liste.objects.get(pk= list_id)

    print(name, amount, status, shop, list)

    Item.objects.create(name = name,
                        amount = amount,
                        status = status,
                        shop = shop,
                        list = list,)

    return HttpResponseRedirect(reverse('myshoppinglist:details'))



def details(request, list_id):
    return HttpResponse("You're looking at list %s." % list_id)
