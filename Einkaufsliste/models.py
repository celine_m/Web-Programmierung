from django.db import models

# Create your models here.

class Shop(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=200)
    amount = models.CharField(max_length=200)
    status_choices =  [
        ("H", "Heute"),
        ("M", "Morgen"),
        ("W", "im Laufe der Woche")
    ]
    status = models.CharField(max_length=200, choices= status_choices,default="T")

    shop = models.ForeignKey(to= Shop, on_delete=models.CASCADE)

    def __str__(self):
        return self.name



