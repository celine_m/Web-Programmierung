from django.shortcuts import render

# Create your views here.
def uhrzeit(request):
    return render(request, "uhrzeit/uhrzeit.html")

import datetime

def uhrzeit(request):
    uhrzeit = datetime.datetime.now()
    context = {
        "uhrzeit": uhrzeit,
    }
    return render(request, "uhrzeit/uhrzeit.html", context)
